//
//  ViewController.swift
//  DrawFlag
//
//  Created by Hui Chih Wang on 2021/3/30.
//

import UIKit
import AVFoundation

class FlagViewController: UIViewController {

    @IBOutlet weak var buttonView: UIButton!
    
    let player1 = AVPlayer()
    let player2 = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        createBackgroundMusic()
        
        let flagView = createFlagView()
        let screenSize = UIScreen.main.bounds.size
        let center = CGPoint(x: screenSize.width / 2, y: screenSize.height / 2)
        flagView.frame = CGRect(x: center.x - 190, y: 100, width: 200, height: 250)
        view.addSubview(flagView)
        
        let emitterView = createMissileBackground()
        view.addSubview(emitterView)
        view.bringSubviewToFront(flagView)
        
        
        let sizeImage = CGSize(width: 160, height: 260)
        let centerImage = CGPoint(x: sizeImage.width / 2, y: view.frame.size.height - sizeImage.height / 2)
        let koreaFishImageView = UIImageView(image: #imageLiteral(resourceName: "koreafish"))
        koreaFishImageView.contentMode = .scaleAspectFill
        view.addSubview(koreaFishImageView)
        
        koreaFishImageView.frame = CGRect(origin: centerImage, size: CGSize.zero)
        view.bringSubviewToFront(koreaFishImageView)
        
        UIView.animate(withDuration: 3) {
            koreaFishImageView.frame = CGRect(origin: CGPoint(x: centerImage.x - sizeImage.width / 2, y: centerImage.y - sizeImage.height / 2), size: sizeImage)
        }
        
        UIView.animate(withDuration: 1.5){
            koreaFishImageView.transform = CGAffineTransform(rotationAngle:  .pi)
            
        }
        
        UIView.animate(withDuration: 1.5, delay: 1.5){
            koreaFishImageView.transform = CGAffineTransform(rotationAngle:  .pi * 2)
            
        }
        
        view.bringSubviewToFront(buttonView)
        
    }
    
    func createBackgroundMusic() {
        if let fileUrl = Bundle.main.url(forResource: "night_attack", withExtension: "mp3") {
            let playerItem = AVPlayerItem(url: fileUrl)
            player1.volume = 1.0
            player1.replaceCurrentItem(with: playerItem)
        }
        
        if let fileUrl = Bundle.main.url(forResource: "review", withExtension: "mp3") {
            let playerItem = AVPlayerItem(url: fileUrl)
            player2.volume = 0.6
            player2.replaceCurrentItem(with: playerItem)
        }

        player1.play()
        player2.play()
    }
    
    
    func createMissileBackground() -> UIView {
        let view = UIView(frame: self.view.frame)
        
        let missileEmitterCells = [
            createEmiiterCell(name: "missile-1", acc: 100),
            createEmiiterCell(name: "missile-2", acc: 30),
            createEmiiterCell(name: "missile-3", acc: 50),
        ]


        let missileEmitterLayer = CAEmitterLayer()
        missileEmitterLayer.emitterPosition = CGPoint(x: -100, y: view.frame.height)
        missileEmitterLayer.emitterSize = CGSize(width: 10, height: view.frame.height - 200)
            missileEmitterLayer.emitterShape = .rectangle
        missileEmitterLayer.emitterCells = missileEmitterCells
        
        view.layer.addSublayer(missileEmitterLayer)
        return view
    }
    
    func createEmiiterCell(name: String, acc: CGFloat = 50) -> CAEmitterCell {
        let cell = CAEmitterCell()
        
        cell.contents = UIImage(named: name)?.cgImage
        cell.birthRate = 1
        cell.lifetime = 10
        cell.yAcceleration = -acc
        cell.xAcceleration = acc
        
        return cell
    }
    
    func createFlagView() -> UIView {
        let view = UIView()
        
        let pathRedRect =  UIBezierPath(rect: CGRect(x: 0, y: 0, width: 380, height: 250))
        let layerRedRect = CAShapeLayer()
        layerRedRect.path = pathRedRect.cgPath
        layerRedRect.fillColor = #colorLiteral(red: 0.8468227983, green: 0.1160614863, blue: 0.02539070323, alpha: 1).cgColor
        addAnimatedStrokeToLayer(layer: layerRedRect, strokeColor: .white)
        view.layer.addSublayer(layerRedRect)
        
        
        let pathBlueRect = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 190, height: 125))
        let layerBlueRect = CAShapeLayer()
        layerBlueRect.path = pathBlueRect.cgPath
        layerBlueRect.fillColor = #colorLiteral(red: 0.04905883223, green: 0.1090988442, blue: 0.5476323962, alpha: 1).cgColor
        addAnimatedStrokeToLayer(layer: layerBlueRect, strokeColor: .white)
        view.layer.addSublayer(layerBlueRect)
        
        let center = CGPoint(x: 95, y: 62.5)
        let pathCircle = UIBezierPath(arcCenter: center, radius: 25, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        let layerCircle = CAShapeLayer()
        layerCircle.path = pathCircle.cgPath
        layerCircle.fillColor = #colorLiteral(red: 0.9999071956, green: 1, blue: 0.999881804, alpha: 1).cgColor
        addAnimatedStrokeToLayer(layer: layerCircle, strokeColor: .black, strokeWidth: 3)
        view.layer.addSublayer(layerCircle)
        
        let trianglePath = UIBezierPath()
        let spacing: CGFloat = .pi / 180 * 2.5
        trianglePath.move(to: CGPoint(x: 140, y: 62.5))
        trianglePath.addArc(withCenter: CGPoint(x: 95, y: 62.5), radius: 27, startAngle: -.pi / 12 + spacing, endAngle: .pi / 12 - spacing, clockwise: true)

        for index in 0..<12 {
            var transform = CGAffineTransform.identity
            transform = transform.translatedBy(x: center.x, y: center.y)
            transform = transform.rotated(by: .pi / 6 * CGFloat(index))
            transform = transform.translatedBy(x: -center.x, y: -center.y)
            
            let layer = CAShapeLayer()
            layer.path = trianglePath.cgPath
            layer.fillColor = #colorLiteral(red: 0.9999071956, green: 1, blue: 0.999881804, alpha: 1).cgColor
            addAnimatedStrokeToLayer(layer: layer, strokeColor: .blue, strokeWidth: 3)
            
            layer.setAffineTransform(transform)
            view.layer.addSublayer(layer)
        }
        
        view.frame.size = CGSize(width: 380, height: 250)
        
        return view
    }
    

    
    private func addAnimatedStrokeToLayer(layer: CAShapeLayer, strokeColor: UIColor = .black, strokeWidth: CGFloat = 5) {
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = strokeWidth
        addAnimationToLayer(layer: layer)
    }
    
    private func addAnimationToLayer(layer: CAShapeLayer) {
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.fromValue = 0
        animation.toValue = 1
        animation.duration = 5
        animation.repeatCount = .infinity
        layer.add(animation, forKey: nil)
    }


}

