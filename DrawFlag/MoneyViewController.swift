//
//  MoneyController.swift
//  DrawFlag
//
//  Created by Hui Chih Wang on 2021/3/31.
//

import UIKit

class MoneyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let gradientView = createGradientView()
        gradientView.center = view.center
        view.addSubview(gradientView)
        
        let moneyView = createMoneyView(frame: view.frame)
        view.addSubview(moneyView)
//        view.sendSubviewToBack(gradientView)

    }
    
    func createGradientView() -> UIView {
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "taiwan"))
        imageView.contentMode = .scaleAspectFit
        let view = UIView(frame: imageView.bounds)
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = view.bounds
        
        gradientLayer.colors = [
            #colorLiteral(red: 0.0431372549, green: 0.6705882353, blue: 0.3921568627, alpha: 1).cgColor,
            #colorLiteral(red: 0.231372549, green: 0.7176470588, blue: 0.5607843137, alpha: 1).cgColor,
        ]
        
        view.layer.addSublayer(gradientLayer)
        view.mask = imageView
        
        return view
    }

    func createMoneyView(frame: CGRect) -> UIView
    {
        let view = UIView(frame: frame)
        
        let cell = CAEmitterCell()
        cell.contents = UIImage(named: "ntd")?.cgImage
        cell.birthRate = 2.5
        cell.lifetime = 10
        cell.yAcceleration = 60
        cell.velocity = 50
        
        let moneyEmitterLayer = CAEmitterLayer()
        moneyEmitterLayer.emitterCells = [cell]
    
        moneyEmitterLayer.position = CGPoint(x: frame.midX, y: frame.minY - 100)
        moneyEmitterLayer.emitterSize = CGSize(width: frame.width, height: 0)
        moneyEmitterLayer.emitterShape = .line
        
        view.layer.addSublayer(moneyEmitterLayer)
        return view
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }

}
